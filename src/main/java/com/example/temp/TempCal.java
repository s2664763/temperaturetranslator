package com.example.temp;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets a Celsius index and returns the Fahrenheit Index
 */

public class TempCal extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Calculation cal;
	
    public void init() throws ServletException {
    	cal = new Calculation();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Translation Result";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Your Celsius Index: " +
                   Double.parseDouble(request.getParameter("temp")) + "\n" +
                "  <P>Your Fahrenheit Index: " +
                   cal.getTemp(request.getParameter("temp")) +
                "</BODY></HTML>");
  }
  

}
