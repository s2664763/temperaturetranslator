package com.example.temp;

import java.util.HashMap;

public class Calculation {
    public double getTemp(String temp) {
        double ctemp = Double.parseDouble(temp);
        double ftemp;
        ftemp = (ctemp*1.8) + 32;
        return ftemp;
    }
}
